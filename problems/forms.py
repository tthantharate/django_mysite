from django import forms
from django.db.models import F
from problems.models import Problem
from tag.models import Tag

class ProblemAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProblemAdminForm, self).__init__(*args, **kwargs)
        self.fields[
            'tags'].queryset = Tag.objects.filter(lft=F('rght')-1) #get all childs

    class Meta:
        model = Problem

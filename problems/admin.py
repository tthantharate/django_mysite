from django.contrib import admin
from django.contrib.admin.filters import SimpleListFilter
from problems.forms import ProblemAdminForm
from problems.models import Problem, Choice
from tag.models import Tag

class TagName(SimpleListFilter):
	title = 'tag name'
	parameter_name = 'tag_id'
	
	def lookups(self, request, model_admin):
		return Tag.objects.all().order_by('name').values_list('id', 'name')

	def queryset(self, request, queryset):
		try:
			tag_obj = Tag.objects.get(pk=self.value())
		except Tag.DoesNotExist:
			return queryset
		tag_id_lst = tag_obj.get_descendants(include_self=True).values_list('id', flat=True)
		return queryset.filter(tags__id__in=tag_id_lst)


class ChoiceInline(admin.TabularInline):
	model = Choice
	extra = 3

class ProblemAdmin(admin.ModelAdmin):
	# order the fields
	# fields = ['create_date', 'question']

	fieldsets = [
		(None, 				 {'fields':['question']}),
		('Date information', {'fields':['create_date'], 'classes':['collapse']}),
		('Covered Concept', {'fields':['tags']}),
	]
	inlines = [ChoiceInline]
	list_display = ('question', 'create_date', 'was_created_recently')
#	list_filter = ['create_date','tags__name']
	list_filter = ['create_date', TagName]
	search_fields = ['question']
	date_hierarchy = 'create_date'
	form = ProblemAdminForm
	
admin.site.register(Problem, ProblemAdmin)

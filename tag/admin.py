from django.contrib import admin
from tag.models import Tag
from treeadmin.admin import TreeAdmin

class TagAdmin(TreeAdmin):
	list_display = ('name',)
	fieldsets = [
		(None, 	{'fields':['parent','name',]}),
	]
	
admin.site.register(Tag, TagAdmin)
